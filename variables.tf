variable region {
  default = "ap-northeast-2"
}

variable "prefix" {
  default = "demo"
}

variable "cidr_block" {
  default = "10.0.0.0/16"
}
